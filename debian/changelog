libtime-progress-perl (2.14-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 2.14.
  * Update debian/upstream/metadata.
  * Drop 001_fix-typo-in-pod.patch, merged upstream.
  * Update years of upstream copyright.
  * Declare compliance with Debian Policy 4.6.2.
  * Set Rules-Requires-Root: no.
  * Add patch from upstream Git repo to fix a warning.
  * Declare compliance with Debian Policy 4.7.0.

 -- gregor herrmann <gregoa@debian.org>  Sun, 08 Sep 2024 17:47:36 +0200

libtime-progress-perl (2.12-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libtime-progress-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 17:41:16 +0100

libtime-progress-perl (2.12-2) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 21:01:08 +0100

libtime-progress-perl (2.12-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 11:27:11 +0100

libtime-progress-perl (2.12-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Nathan Handler from Uploaders. Thanks for your work!

  [ Lucas Kanashiro ]
  * Import upstream version 2.12
  * Bump debhelper compatibility level to 10
  * Declare compliance with Debian Policy 4.0.0
  * Create patch to fix typo in POD

 -- Lucas Kanashiro <kanashiro@debian.org>  Fri, 23 Jun 2017 19:18:06 -0300

libtime-progress-perl (2.11-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 2.11
  * Update long description

 -- Lucas Kanashiro <kanashiro.duarte@gmail.com>  Tue, 08 Dec 2015 09:53:32 -0200

libtime-progress-perl (2.0-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * Imported Upstream version 2.0
  * Update copyright years for upstream files
  * Add debian/upstream/metadata
  * Add myself to Uploaders
  * Update copyright years for debian/* packaging files
  * Wrap and sort fields in debian/control file
  * Declare compliance with Debian policy 3.9.6
  * Declare package as autopkgtestable

 -- Salvatore Bonaccorso <carnil@debian.org>  Sun, 27 Sep 2015 08:02:28 +0200

libtime-progress-perl (1.8-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Email change: Salvatore Bonaccorso -> carnil@debian.org

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Nathan Handler ]
  * Email change: Nathan Handler -> nhandler@debian.org

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs
  * Update debian/copyright file information.
    Update format to copyright-format 1.0 as released together with Debian
    policy 3.9.3.
    Update copyright years for debian/* packaging files.
  * Update Debhelper compat level to 9.
    Adjust versioned Build-Depends on debhelper to (>= 9).
  * Bump Standards-Version to 3.9.4

 -- Salvatore Bonaccorso <carnil@debian.org>  Mon, 08 Jul 2013 22:58:07 +0200

libtime-progress-perl (1.7-1) unstable; urgency=low

  * Team upload.
  * New upstream release
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).
  * debian/rules: Simplify to a three line rules file.
  * Convert to '3.0 (quilt)' source package format.
  * Refresh debian/copyright file: Use revision 135 of format-
    specification in DEP5 for machine-readable copyright file
    information.
  * Bump Standards-Version to 3.9.1.

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Fri, 10 Sep 2010 14:02:53 +0200

libtime-progress-perl (1.5-1) unstable; urgency=low

  * Initial Release. (closes: #535137) (LP: #318626)

 -- Nathan Handler <nhandler@ubuntu.com>  Tue, 30 Jun 2009 02:26:39 +0000
